#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_rpmbrew-tools
----------------------------------

Tests for `rpmbrew.decorators` module.
"""

import unittest

from rpmbrew.decorators import coerce_params



class TestDecorators(unittest.TestCase):
    
    def test_convert_params(self):
        from pathlib import Path
        
        @coerce_params({"path":Path})
        def test_func(path=None):
            print(path)
            return path
        
        
        p = test_func(path="/var/lib/")
        
        self.assertIsInstance(p, Path)
        
        #Raises TypeError if path param is None
        self.assertRaises(TypeError, test_func, path=None)
    
        
        
if __name__ == '__main__':
    unittest.main()
