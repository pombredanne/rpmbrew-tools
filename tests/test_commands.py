#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, absolute_import, unicode_literals

"""
test_rpmbrew-tools
----------------------------------

Tests for `rpmbrew` module.
"""

import unittest
import pathlib
import sys
from shutil import rmtree


from rpmbrew.brew import Brew
from rpmbrew.commands import repo_parse
from rpmbrew.commands import clone_from_git
from rpmbrew.commands import ParsedRepoUrl
from rpmbrew.commands import list_specfiles


class TestShortcuts(unittest.TestCase):


    def test_repo_parse(self):
        """
        Git accepts different format of urls as follows:

        * ssh://user@host.xz:port/path/to/repo.git/
        * ssh://user@host.xz/path/to/repo.git/
        * ssh://host.xz:port/path/to/repo.git/
        * ssh://host.xz/path/to/repo.git/
        * ssh://user@host.xz/path/to/repo.git/
        * ssh://host.xz/path/to/repo.git/
        * ssh://user@host.xz/~user/path/to/repo.git/
        * ssh://host.xz/~user/path/to/repo.git/
        * ssh://user@host.xz/~/path/to/repo.git
        * ssh://host.xz/~/path/to/repo.git
        * user@host.xz:/path/to/repo.git/
        * host.xz:/path/to/repo.git/
        * user@host.xz:~user/path/to/repo.git/
        * host.xz:~user/path/to/repo.git/
        * user@host.xz:path/to/repo.git
        * host.xz:path/to/repo.git
        * rsync://host.xz/path/to/repo.git/
        * git://host.xz/path/to/repo.git/
        * git://host.xz/~user/path/to/repo.git/
        * http://host.xz/path/to/repo.git/
        * https://host.xz/path/to/repo.git/
        * /path/to/repo.git/
        * path/to/repo.git/
        * ~/path/to/repo.git
        * file:///path/to/repo.git/
        * file://~/path/to/repo.git/

        """



        #Test we can parse ssh github
        repo_url = "git@github.com:rpmbrew/apr.git"

        parse_result = repo_parse(repo_url)
        self.assertTrue(isinstance(parse_result, ParsedRepoUrl ))
        self.assertEqual(parse_result.netloc, "github.com")
        self.assertEqual(parse_result.path, "/rpmbrew/apr.git")
        self.assertEqual(parse_result.name, "apr")

        #Test we can parse https github
        repo_url =  "https://github.com/rpmbrew/apr.git"

        parse_result = repo_parse(repo_url)
        self.assertTrue(isinstance(parse_result, ParsedRepoUrl ))
        self.assertEqual(parse_result.netloc, "github.com")
        self.assertEqual(parse_result.path, "/rpmbrew/apr.git")
        self.assertEqual(parse_result.name, "apr")


        #test we can parse bitbucket ssh
        repo_url = """git@bitbucket.org:jnvilo/rpmbrew-tools.git"""
        parse_result = repo_parse(repo_url)
        self.assertTrue(isinstance(parse_result, ParsedRepoUrl ))
        self.assertEqual(parse_result.netloc, "bitbucket.org")
        self.assertEqual(parse_result.path,"/jnvilo/rpmbrew-tools.git")
        self.assertEqual(parse_result.name, "rpmbrew-tools")

        #test we can parse bitbucket https

        repo_url = """https://jnvilo@bitbucket.org/jnvilo/rpmbrew-tools.git"""
        parse_result = repo_parse(repo_url)
        self.assertTrue(isinstance(parse_result, ParsedRepoUrl ))
        self.assertEqual(parse_result.netloc, "bitbucket.org")
        self.assertEqual(parse_result.path,"/jnvilo/rpmbrew-tools.git")
        self.assertEqual(parse_result.name, "rpmbrew-tools")




    def test_clone_from_git(self):
        """Ensures that the clone_from_git shortcut can clone"""

        git_repo_url = "https://github.com/rpmbrew/apr.git"
        clone_from_git(git_repo_url, top_dir="/var/tmp/apr")





if __name__ == '__main__':
    unittest.main()