#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_rpmbrew-tools
----------------------------------

Tests for `rpmbrew` module.
"""

import unittest

from rpmbrew.brew import Brew
from rpmbrew.brew import get_os_dist
import pathlib
import sys
from shutil import rmtree


from utils import clean_builddir

class TestBrewFunctions(unittest.TestCase):
    
    def test_get_os_dist(self):
        

        dist = get_os_dist()
        self.assertEqual(dist, "fc21")
        


class TestBrew(unittest.TestCase):
    """Tests the class rpmbrew.Brew"""

    def setUp(self):

        self.top_dir = pathlib.Path("/var/tmp/builddir")
        self.repo_url = "https://github.com/rpmbrew/apr.git"
        self.spec = "apr.spec"
        self.dist = ".el6"

        self.brew = Brew(self.top_dir, 
                         self.repo_url,
                         dist=self.dist,
                         spec=self.spec)




    def test_properties(self):

        #Test that the parameters are being set properly
        self.assertEqual(self.spec, self.brew.spec)
        self.assertEqual(self.top_dir, self.brew.top_dir)
        self.assertEqual(self.dist, self.brew.dist)
      

        self.assertEqual(self.brew.RPMS_path, pathlib.Path(self.top_dir,"RPMS"))
        self.assertEqual(self.brew.SRPMS_path, pathlib.Path(self.top_dir, "SRPMS"))
        self.assertEqual(self.brew.SOURCES_path,pathlib.Path( self.top_dir,"SOURCES"))
        self.assertEqual(self.brew.SPECS_path, pathlib.Path(self.top_dir, "SPECS"))


    def test_clone(self):

        clean_builddir(self.brew.top_dir.as_posix())
        self.brew.clone()

        #Ensure we have the directories checked  out.

        self.assertTrue(self.brew.SPECS_path.is_dir())
        self.assertTrue(self.brew.SOURCES_path.is_dir())
        self.assertTrue(pathlib.Path(self.brew.top_dir,"README.md").is_file())

        #lets clean up 
        #clean_builddir(self.brew.top_dir.as_posix())

    def test_download_sources(self):
        clean_builddir(self.brew.top_dir.as_posix())
        self.brew.clone()        
        self.brew.download_sources()

        #We just check if the apr-1.5.1.tar.bz2 exists. HardCoded!!
        
        src_file = "apr-1.5.1.tar.bz2"
        
        path_src_file = pathlib.Path(self.brew.SOURCES_path, src_file)
        self.assertTrue(path_src_file.is_file())
        


    def test_buildrequires(self):
        clean_builddir(self.brew.top_dir.as_posix())
        self.brew.clone()            
        
        #This test will work only on Fedora!!
        known_reqs = ["autoconf", "libtool", "libuuid-devel", "python", "lksctp-tools-devel"]
        
        for requirement in known_reqs:
            "Checking if {} is in the buildrequires list....".format(requirement)
            self.assertTrue(requirement in self.brew.buildrequires)
            
    def test_buildrpms(self):
        
        """This tests that we can do a full rpmbuild."""
        
        self.brew.clone()
        self.brew.install_deps()
        self.brew.download_sources()
        self.brew.build_rpms()
            
           

    def tearDown(self):
        #clean_builddir(self.top_dir.as_posix())
        pass
    
if __name__ == '__main__':
    unittest.main()
