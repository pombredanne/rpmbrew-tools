from shutil import rmtree

def clean_builddir(path):

    #Ensure that the top_dir is empty 
    try:
        rmtree(path)        
    except OSError as e:
        if e.errno == 2:
            #The directory does not exist. We have nothing to do.
            pass
        else:
            #Something horrible has happened with deleting the directory.
            #Throw the exception again.
            raise e