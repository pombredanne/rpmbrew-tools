from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import


import sys
import os
import argparse

#def oldmain():
    
    #arguments = docopt(__doc__, version='RPMBrew Tool 0.1')
    #print(arguments)

    #if arguments.get("tar2rpm",None):
        #filename = arguments.get("<filename>")
        #source = arguments.get("--source")
        #prefix= arguments.get("--prefix")
       
        #tar2rpm(filename=filename, prefix=prefix,
                        #source=source)
    #if arguments.get("createrepo",None):
        #createrepo_rpmbuild()        

    #if arguments.get("makerepo", None):
        #make_repo()

    #if arguments.get("setuptree", None):
        #setuptree()

#def do_build():
    
    #pass



#def main(argv=sys.argv[1:]):
    
    #parser = argparse.ArgumentParser()
    #subparsers = parser.add_subparsers()
    #clone = subparsers.add_parser('clone',)
    #clone.add_argument('repo_url')
    #installdeps = subparsers.add_parser('installdeps')
    
    #args = parser.parse_args(argv)
    ##args = parser.parse_args(["clone", "bar"])
    #print(args)


def get_tty_name():
    
    fd = os.open("/dev/tty", os.O_RDONLY)
    p = os.ttyname(fd)
    os.close(fd)
    return p
    
    
class RPMBrewCommandLine(object):

    def __init__(self):
        parser = argparse.ArgumentParser(
            description='RPMBrew Command Line Tool',
            usage='''rpmbrew <command> [<args>]

The most commonly used git commands are:
   clone      Clone an RPMBrew Repo
   commit     Record changes to the repository
   fetch      Download objects and refs from another repository
''')
        parser.add_argument('command', help='Subcommand to run')
        # parse_args defaults to [1:] for args, but you need to
        # exclude the rest of the args too, or validation will fail
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print('Unrecognized command')
            parser.print_help()
            exit(1)
        # use dispatch pattern to invoke method with same name
        getattr(self, args.command)()
        
        
        
    def clone(self):
        parser = argparse.ArgumentParser(
        description="Clone an RPMBrew git Repo.")        
        parser.add_argument('repository')
        args = parser.parse_args(sys.argv[2:])
        #print 'Running git commit, amend=%s' % args.amend
        
        #This must be a git repo format.
        # https://github.com/rpmbrew/apr.git
        # git@github.com:rpmbrew/apr.git
        # ssh://git@git.mit.corp-apps.com:7999/unix/python-foreman-api.git
        

        https_regex = r"""https://[/a-z\.\d]*.git"""
        ssh_regex = r"""(ssh://)?git@[-\da-z\.]*:[/a-z\d\.]*.git"""        
        #The remote repo must pass the above regex test
        
        
        
        
        
        
        
        

        
        
        print(args)
        
        
    def commit(self):
        parser = argparse.ArgumentParser(
            description='Record changes to the repository')
        # prefixing the argument with -- means it's optional
        parser.add_argument('--amend', action='store_true')
        # now that we're inside a subcommand, ignore the first
        # TWO argvs, ie the command (git) and the subcommand (commit)
        args = parser.parse_args(sys.argv[2:])
        print("Running git commit, amend={}".format(args.amend))

    def fetch(self):
        parser = argparse.ArgumentParser(
            description='Download objects and refs from another repository')
        # NOT prefixing the argument with -- means it's not optional
        parser.add_argument('repository')
        args = parser.parse_args(sys.argv[2:])
        print("Running git fetch, repository={}".format(args.repository))


def main():
    
    RPMBrewCommandLine() 
  
if __name__ == "__main__":
    
    RPMBrewCommandLine()    
    
    
        