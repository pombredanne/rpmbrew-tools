repo_template = """
[{REPONAME}]
name={DESCRIPTION}
baseurl={BASEURL}
enabled=1
gpgcheck=0
"""

tar_spec_template = """
%define prefix {PREFIX}

Name:     {NAME}        
Version:  {VERSION}      
Release:  {RELEASE} 
Summary:  {SUMMARY}       

License:  {LICENSE}      
URL:      {URL}     
Source0:  {SOURCE}      

BuildRoot:  %{{_tmppath}}/%{{name}}-%{{version}}-%{{release}}-root-%(%{{__id_u}} -n)

#BuildRequires:  
#Requires:       

%description
{DESCRIPTION}
%prep
rm -rf $RPM_BUILD_DIR/%{{name}}-%{{version}}
 
%install
mkdir -p $RPM_BUILD_ROOT/%{{prefix}}
tar xfzC $RPM_SOURCE_DIR/%{{name}}-%{{version}}.tar.gz $RPM_BUILD_ROOT/%{{prefix}}

%files
%{{prefix}}/%{{name}}-%{{version}}
%doc

%changelog
"""

spec_template = tar_spec_template

rpmmacros = """
#default build macros here.

%_topdir %(echo $HOME)/rpmbuild\n
\n
%_smp_mflags %( 
    [ -z "$RPM_BUILD_NCPUS" ] \\\   \n
        && RPM_BUILD_NCPUS="`/usr/bin/nproc 2>/dev/null || \\\  
                             /usr/bin/getconf _NPROCESSORS_ONLN`"; \\\
    if [ "$RPM_BUILD_NCPUS" -gt 16 ]; then \\\
        echo "-j16"; \\\
    elif [ "$RPM_BUILD_NCPUS" -gt 3 ]; then \\\
        echo "-j$RPM_BUILD_NCPUS"; \\\
    else \\\
        echo "-j3"; \\\
    fi )

%__arch_install_post \
    [ "%{buildarch}" = "noarch" ] || QA_CHECK_RPATHS=1 ; \
    case "${QA_CHECK_RPATHS:-}" in [1yY]*) /usr/lib/rpm/check-rpaths ;; esac \
    /usr/lib/rpm/check-buildroot

"""


newpackage = """
%define prefix {PREFIX}

Name:     {NAME}        
Version:  {VERSION}      
Release:  {RELEASE} 
Summary:  {SUMMARY}       

License:  {LICENSE}      
URL:      {URL}     
Source0:  {SOURCE}      

BuildRoot:  %{{_tmppath}}/%{{name}}-%{{version}}-%{{release}}-root-%(%{{__id_u}} -n)

#BuildRequires:  
#Requires:       

%description
{DESCRIPTION}
%prep
rm -rf $RPM_BUILD_DIR/%{{name}}-%{{version}}
 
%install
mkdir -p $RPM_BUILD_ROOT/%{{prefix}}
tar xfzC $RPM_SOURCE_DIR/%{{name}}-%{{version}}.tar.gz $RPM_BUILD_ROOT/%{{prefix}}

%files
%{{prefix}}/%{{name}}-%{{version}}
%doc

%changelog
"""