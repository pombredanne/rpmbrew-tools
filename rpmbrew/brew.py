# -*- coding: utf-8 -*-

from pathlib import Path
import shutil
import os
import sys
import platform
from git import Repo
from subprocess import Popen, PIPE, STDOUT

from rpmbrew.exceptions import UnsupportedBuildOS

try:
    from sh import rpmbuild
except ImportError as e:
    print("rpmbuild not found.")
    sys.exit(status=1)
try:
    from sh import spectool
except ImportError as e:
    print("spectool not found.")
    sys.exit(status=1)

try:
    from sh import rpmspec
except ImportError as e:
    print("rpmspec tool not found.")
    sys.exit(status=1)



def get_os_dist():
    """
    Finds the default dist string for the OS. Currently works only on linux.
    """

    if platform.system() == "Linux":
        x = platform.release().split(".")
        dist = x[len(x)-2]
        return dist
    else:
        raise UnsuportedBuildOS("Your system dist could not be automatically detected.")


def create_brew(top_dir=None, ):
    """
    As a user I want to be able to type:

    1) rpmbrew init mypackage.spec  - Automatically create a directory
    ./mypackage/{SOURCES, SPECS} and put ./mypackage/SPECS/mypackage.spec
    with defaults.

    2) rpmbrew init mypackage.spec -v 0.0.1 --author "Jason Viloria --email="jnvilo.gmail.com" --license=gnu
    will do 1 and also update the spec file with the parameters I have passed.


    3) rpmbrew init https://github.com/foo/mypackage.git --dist=sys.el6 - Will instead do a
    git clone and create the branch sys.el6

    """

    pass


class Brew(object):
    """Encapsulates an rpmbrew RPM Build. This is a low level class that
    implements the required functionality. It does not do any input
    validation or error recovery. Throws exceptions."""

    def __init__(self,top_dir, repo_url, dist=None, spec=None):
        self._top_dir = Path(top_dir)
        self._repo_url = repo_url
        self.spec = spec

        if not dist:
            dist = get_os_dist()

        if not dist.startswith("."):
            dist = "." + dist
        self._dist = dist

    @property
    def dist(self):
        return self._dist

    @property
    def top_dir(self):
        return Path(self._top_dir)

    @property
    def SPECS(self):
        """Returns a list of .spec files from {top_dir}/SPECS"""
        specs = self.SPECS_path.glob("*.spec")
        return specs

    @property
    def SOURCES_path(self):
        return Path(self.top_dir, "SOURCES")

    @property
    def SPECS_path(self):
        p =  Path(self.top_dir, "SPECS")
        return p

    @property
    def RPMS_path(self):
        return Path(self.top_dir, "RPMS")

    @property
    def SRPMS_path(self):
        return Path(self.top_dir, "SRPMS")

    @property
    def spec_filename(self):

        if self.spec is None:
            #User did not give us the name of the file
            #We can just check {top_dir}/SPECS and see if there is a specfile.


            specs_list = list(self.SPECS)

            if len(specs_list) >1:
                raise ValueError("Found more than 1 spec file while trying to figure out what spec file to use.")
            else:
                if len(specs_list) == 0:
                    raise ValueError("No specs file found.")
            return specs_list[0]
        else:
            if self.spec.endswith(".spec"):
                specfilename = self.spec
            else:
                specfilename = self.spec + ".spec"
            return Path(self.SPECS_path,specfilename)



    @property
    def buildrequires(self):

        package_names = []

        def process_output(line):
            line = line.rstrip("\n")
            package_names.append(line)


        topdir = "_topdir {}".format(self.top_dir)
        cmd = """/usr/bin/rpmspec -q --buildrequires {}""".format(self.spec_filename.as_posix())
        p = Popen(cmd, stdout = PIPE, stderr = STDOUT, shell = True)

        for line in iter(p.stdout.readline, ""):
            process_output(line)

        return package_names


    def clone(self):
        """Clones the git repo for this build into top_dir"""

        git_url = self._repo_url


        repo_dir = self._top_dir

        path = Path(repo_dir)

        if path.exists():

            if path.is_dir():
                print("Info: directory {} exists. Going to remove it.".format(repo_dir))
                shutil.rmtree(path.as_posix())

            elif path.is_file():
                print("Info: found a file {} instead of a directory. Removing it.")
                os.remove(path.as_posix)
            else:
                msg = "Unknown file that can not be removed found while trying to clone."
                raise Exception(msg)

        print("Cloning repository: {} into {}".format(git_url, repo_dir))
        Repo.clone_from(git_url,repo_dir.as_posix())
        print("Done..")


    def download_sources(self):
        """
        Uses spectool to parse the specfile and ensures that the sources
        are downloaded into {top_dir}/SOURCES
        """
        #look for the specfile.

        def process_output(line):
            print(line)

        result = spectool(self.spec_filename.as_posix(), "-gf","-C", self.SOURCES_path.as_posix(),_out=process_output)
        print(result)


    def install_deps(self):
        """Installs the required RPMS for the build. This is just here as
        a convenience but it should be done outside of the tool to account
        for buildsets where a dependency might need to be built first.
        """

        for package in self.buildrequires:
            """
            TODO: Add error handling. We can not assume that this
            cmd will always return succesful. Another idea is also
            when we start to build package dependencies.
            """
            cmd = "sudo yum -y install {}".format(package)
            p = Popen(cmd, stdout = PIPE, stderr = STDOUT, shell = True)
            for line in iter(p.stdout.readline, ""):
                print(line)


    def build_rpms(self):

        def process_output(line):

            if line.startswith("Wrote: {}".format(self.SRPMS_path.as_posix())):
                #This is of the format: tmp/foo/bar/SRPMS/apr-1.5.1-3.fc19.src.rpm
                #so we cn save this to our build list as a result.

                print(""""=================================================""")
                print("Saved the SRPMS")


            if line.startswith("Wrote: {}".format(self.RPMS_path.as_posix())):
                #This is of the format: tmp/foo/bar/SRPMS/apr-1.5.1-3.fc19.src.rpm
                #so we cn save this to our build list as a result.

                print(""""=================================================""")
                print("Saved the RPMS")


            print(line)

        topdir = "_topdir {}".format(self.top_dir)
        cmd = """/usr/bin/rpmbuild -ba {} --define="_topdir {}" """.format(self.spec_filename.as_posix(),self.top_dir)

        if self.dist:
            cmd = cmd + """ --define="dist {}"  """.format(self.dist)

        from subprocess import Popen, PIPE, STDOUT

        p = Popen(cmd, stdout = PIPE, stderr = STDOUT, shell = True)

        for line in iter(p.stdout.readline, ""):
            process_output(line)

        print(p)





if __name__ == "__main__":

    brew = Brew(top_dir="/tmp/foo/bar", repo_url="https://github.com/rpmbrew/apr.git", dist="ito.el6", name="apr.spec")
    brew.clone()
    brew.install_deps()
    brew.download_sources()
    brew.build_rpms()

    print("Completed")


    #rpmbrew
    #ssh git@github.com:rpmbrew/apr.git
    # or https://github.com/rpmbrew/apr.git