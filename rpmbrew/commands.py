from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

import re
import os
import pathlib
from collections import namedtuple
from rpmbrew.exceptions import BrewGitUrlException
from rpmbrew.exceptions import InvalidGitUrl
from rpmbrew.brew import Brew
from rpmbrew.decorators import coerce_params

from rpmbrew.specfiles import find_top_dir

ParsedRepoUrl = namedtuple('ParsedRepoUrl', ['scheme', 'netloc' , 'path','port','name'], verbose=False)


def repo_parse(repo_url):

    """
    A shameless copy of urlparse, but in staed parses the git URI and
    returns a ParsedRepoUrl,

    Git accepts different format of urls as follows:

    * ssh://user@host.xz:port/path/to/repo.git/
    * ssh://user@host.xz/path/to/repo.git/
    * ssh://host.xz:port/path/to/repo.git/
    * ssh://host.xz/path/to/repo.git/
    * ssh://user@host.xz/path/to/repo.git/
    * ssh://host.xz/path/to/repo.git/
    * ssh://user@host.xz/~user/path/to/repo.git/
    * ssh://host.xz/~user/path/to/repo.git/
    * ssh://user@host.xz/~/path/to/repo.git
    * ssh://host.xz/~/path/to/repo.git
    * user@host.xz:/path/to/repo.git/
    * host.xz:/path/to/repo.git/
    * user@host.xz:~user/path/to/repo.git/
    * host.xz:~user/path/to/repo.git/
    * user@host.xz:path/to/repo.git
    * host.xz:path/to/repo.git
    * rsync://host.xz/path/to/repo.git/
    * git://host.xz/path/to/repo.git/
    * git://host.xz/~user/path/to/repo.git/
    * http://host.xz/path/to/repo.git/
    * https://host.xz/path/to/repo.git/
    * /path/to/repo.git/
    * path/to/repo.git/
    * ~/path/to/repo.git
    * file:///path/to/repo.git/
    * file://~/path/to/repo.git/


    TODO: Implement parsing all of the above. For now we handle only a few
    basic cases.
    """


    regex_list = [

        #ssh://user@host.xz:port/path/to/repo.git/
        #ssh://user@host.xz/path/to/repo.git/
        #
        r"""(?P<scheme>(ssh://)|(https://))((?P<name>[-a-z\d]*)@)?(?P<netloc>[a-z\.]*)(:(?P<port>[\d]*))?(?P<path>([-\a-z/]*).git)""",
        r"""(?P<scheme>ssh://)?git@(?P<netloc>[-\da-z\.]*):(?P<port>[\d]*)(?P<path>[-/a-z\d\.]*.git)""",
        r"""(?P<scheme>https://)((?P<name>[-a-z\d]*)@)?(?P<netloc>[a-z\.]*)(:(?P<port>[\d]*))?(?P<path>([-\a-z/]*).git)"""
    ]

    for regex_str in regex_list:
        compiled_regex = re.compile(regex_str)
        regex_result = compiled_regex.search(repo_url)

        if regex_result:
            break

    else:
        raise InvalidGitUrl("{} is not a valid git url.".format(repo_url))


    # Retrieve group(s) by name
    scheme = regex_result.group('scheme')
    netloc = regex_result.group('netloc')
    path = regex_result.group('path')
    port = regex_result.group('port')

    if not path.startswith("/"):
        path = "/"+path

    """
    We expect that the path is of the format: /foo/bar/baz.git
    Here we try to extract baz
    """
    i = path.rfind("/")
    if i == -1:
        #We didnt find a /. That is bad and should never occur.
        raise InvalidGitUrl("Could not extract the name of the git repo.")

    name = path[path.rfind("/")+1:]
    i = name.rfind(".")

    if i == -1:
        #We didnt find a .git That is bad and should never occur.
        raise InvalidGitUrl("Could not extract the name of the git repo.")

    name = name[:i]

    return ParsedRepoUrl(scheme=scheme,netloc=netloc,port=port,path=path, name=name)

def clone_from_git(git_repo_url, top_dir=None):
    """
    A shortcut to using rpmbrew.brew.Brew with just providing a
    git repo.
    """

    parsed_repo  = repo_parse(git_repo_url)

    if top_dir is None:
       #This means top_dir is where we are.
        top_dir = pathlib.Path(os.getcwd())


    name = parsed_repo.name

    if not top_dir.endswith(parsed_repo.name):
        top_dir = pathlib.Path(top_dir, name)



    brew = Brew(top_dir, git_repo_url)
    brew.clone()




def download_sources(top_dir=None, name=None):

    """
    When use invokes download command, this function is called.
    It requires  that the user is in a brew repo.
    """

    #We need to find the top_dir.

    if top_dir is None:
        path = pathlib.Path(os.getcwd())
        top_dir = find_top_dir(path)


    brew = Brew(top_dir, None)
    brew.download_sources()



def main():
    path="/foo/bar"
    w(path=path, name="jason")

if __name__ == "__main__":

    main()
