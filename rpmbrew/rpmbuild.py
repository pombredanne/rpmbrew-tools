from pathlib import Path, PosixPath
from os.path import expanduser
import re
import shutil
import sys
import os

from rpmbrew.mirrors import RawhideMirror
from rpmbrew.brewlib import setuptree
from rpmbrew.templates import tar_spec_template
from rpmbrew.exceptions import FileDoesNotExist
from rpmbrew.exceptions import NonStandardTarFileName

class BrewLibException(Exception):
    pass

class RPMDirExistsAsFileException(Exception):
    pass

class RPMSpec(object):
    """Builds a skeleton spec file"""
    
    def __init__(self, name, 
                 version=None,
                 release="%{?dist}",
                 license="GPL",
                 url=None,
                 source=None):
        
        self.name = name
        self.version = version
        self.release = release
        self.license = license
        self.url = url
        self.source = source
        
        
    
class TarRPMSpec(RPMSpec):
    
    """Creates specfile for a tar file so that
    It can be deployed via rpm."""
    
    def __init__(self, tarfile, 
                 rpmbuildpath=None,
                 name = None,
                 version=None,
                 release=1,
                 prefix="/usr/local",
                 summary="No Summary provided",
                 license="No License Provided",
                 url=None,
                 description="No Description provided"):
        
        self.tarfile_path_obj = Path(tarfile)
        if url is None:
            self.url = "http://example.com/{}".format(tarfile)
        else:
            self.url = url
        
        self.prefix=prefix
        self.summary=summary
        self.license=license
        self.url=url
        self.name = name
        self.version = version
        self.release = "{}{}".format(release,"%{?dist}")
        self.source = Path(tarfile).name
        self.description=description
        
        if not self.tarfile_path_obj.exists():
            raise FileDoesNotExist("{} does not exist.".format(self.tarfile_path_obj.name))
            
        
        if rpmbuildpath is None:
            self.rpmbuildpath = RPMBuildPath(Path(expanduser("~"), "rpmbuild"))
        else:
            self.rpmbuildpath = rpmbuildpath
        if self.name is None:
            name, major, minor = self.__filename_attributes__(self.tarfile_path_obj.name)
            self.name = name
            self.version = "{}.{}".format(major,minor)
            
        
    def __get_or_create_spec__(self):
        
        return tar_spec_template.format(PREFIX=self.prefix,
                                        NAME=self.name,
                                        VERSION=self.version,
                                        RELEASE=self.release,
                                        SUMMARY=self.summary,
                                        LICENSE=self.license,
                                        URL=self.url,
                                        SOURCE=self.source,
                                        DESCRIPTION=self.description
                                        )
    
    def __str__(self):
        return self.__get_or_create_spec__()
    
    def __unicode__(self):
        return self.__get_or_create_spec__()
    
    def __filename_attributes__(self, filename):
        """
        Splits the filename to name, major, minor.
        Expects to get a format foo-1.2.tar.gz  or foo-1.2.3.tar.gz
        """
        
        
        filename = self.tarfile_path_obj.name
        
        rawstr = r"""(?P<name>[a-z\d_]*)-(?P<major>[\d]*).(?P<minor>[\d]*).tar.gz"""
        matchstr = filename
        match_obj = re.search(rawstr, matchstr)
        
        #TODO: update this to try different regex
        #so that we can support possible tar.gz file formats
        if match_obj:
        
            name = match_obj.group('name')
            major = match_obj.group('major')
            minor = match_obj.group('minor')  
            return (name,major,minor)
        
        else:
            
            rawstr = r"""(?P<name>[a-z\d_]*)-(?P<major>[\d]*).(?P<minor>[\d]*.[\d]*).tar.gz"""
            matchstr = filename
            match_obj = re.search(rawstr, matchstr)
            
            if match_obj:
                   
                name = match_obj.group('name')
                major = match_obj.group('major')
                minor = match_obj.group('minor')  
                return (name,major,minor)
            
        raise NonStandardTarFileName("{} is non standard.".format(filename))
    
    
    def save(self):
        

        with self.path.open(mode="w+") as f:
            f.write(self.__get_or_create_spec__())
            
    @property        
    def path(self):
        output_filename = self.name + ".spec"
        return Path(self.rpmbuildpath.SPEC_path, output_filename) 
        
    
class RPMMacros(object):
    
    def __init__(self, path):
        
        self.__path_obj__ = Path(path)
        
    def save(self):
        
        from rpmbrew.templates  import rpmmacros
        
        with self.__path_obj__.open("w+") as f:
            f.write(rpmmacros)
    
    
    def set_define(name,value):
        pass
    
    
    def get_define(name,value):
        pass
    
    
    def update_rpmmacros(self):
         
        """ 
        if rpmmacros_path_ojb.exists():
            with rpmmacros_path_ojb.open() as f:  
                lines = f.readlines()
                 
        else: 
            from rpmbrew.templates  import rpmmacros
            lines = rpmmacros
             
        newlines = []        
        for line in lines: 
           
            if line.startswith("%_topdir"):
                newlines.append("%_topdir\t{}".format(self.path_obj))
     
            else:
                newlines.append(line)
             
        text = "".join(newlines)
        print(text)
        """
        pass
class RPMBuildPath(object):
    """A class that manages a local rpmbuild directory."""
    
    
    def __init__(self, rpmbuild_path):
        
        self.rpmbuild_path = rpmbuild_path
        
        if isinstance(rpmbuild_path, RPMBuildPath):
            rpmbuild_path = str(rpmbuild_path.path_obj)
        
        self.path_obj = Path(rpmbuild_path)
        self.__rpmmacros_obj__ = Path(expanduser("~"), ".rpmmacros")        
    
    @property
    def topdir(self):
        return self.path_obj
    @property
    def rpmmacros(self):
        return self.__rpmmacros_obj__
        

    def exists(self):
        return self.path_obj.exists()

    def mkdir(self):
        #self.path_obj.mkdir()
        os.makedirs(self.path_obj.as_posix())
    
    def setupdirs(self):
            
            
        if not self.path_obj.exists():
            os.makedirs(str(self.path_obj))
            
        for each in ["BUILD","RPMS","SOURCES","SPECS","SRPMS"]:
            builddir_path = Path(self.path_obj, each)
            if not builddir_path.exists():
                builddir_path.mkdir()
                
        #else:
        #    raise FileExistsError("File exists: {}".format(builddir_path))
            
    def __str__(self):
        return str(self.path_obj)
    
    @property
    def SRPMS_path(self):        
        return Path(self.path_obj, "SRPMS")
    
    @property
    def RPMS_path(self):
        return Path(self.path_obj, "RPMS")
    
    @property 
    def SPEC_path(self):
        return Path(self.path_obj, "SPECS")
    
    @property
    def SOURCES_path(self):
        return Path(self.path_obj, "SOURCES")

 
class RPMBuildManager(object):
    """Gets a list of rpms and builds them"""
    
    def __init__(self):
        pass
    

def sh_print(line):
    
    print(line)
    
class Tar2RPMBuildManager(object):
    
    def __init__(self, filename, rpmbuildpath=None):
        
        self.filename = filename
        
        if rpmbuildpath is None:
            #Set the rpmbuild dir in home directory as a default build location
            self.rpmbuildpath_obj = RPMBuildPath(Path(expanduser("~"), "rpmbuild"))
        else:
            self.rpmbuildpath_obj = RPMBuildPath(rpmbuildpath)
            
        #TODO: Add some sanity checks on the filename
        self.specfile_manager = TarRPMSpec(filename, rpmbuildpath=self.rpmbuildpath_obj)
        
        
            
    def prep(self):
        """
        Prepares the build environment. Ensures that the tarfile is 
        copied to the source directory if it is not there yet.
        
        Optionaly should rename the tar file if such options are given.
        """
        if not self.rpmbuildpath_obj.exists():
            self.rpmbuildpath_obj.setupdirs()
        
        destination = str(self.rpmbuildpath_obj.SOURCES_path)
        source = str(self.filename)
        print(source,destination)
        shutil.copy(source, destination)
        self.specfile_manager.save()
        
        
    def build(self):
        """
        Executes the actual rpmbuild process.
        """
        try:
            from sh import rpmbuild
        except ImportError as e:
            print("rpmbuild executable not found. Can not build.")
            sys.exit(status=1)
        
        print("Starting to build {} in {}".format(self.filename, self.rpmbuildpath_obj.path_obj))    
        print("Specfile: {}".format(self.specfile_manager.path))
        
        defines = "_topdir {}".format(str(self.rpmbuildpath_obj.topdir))
        
        p = rpmbuild(self.specfile_manager.path, "--define", defines, "-ba", _out=sh_print, _err=sh_print)
        p.wait()
        print("Built {}".format(self.rpmbuildpath_obj))
    
    
    
    
    
        
        

            
        
    
    