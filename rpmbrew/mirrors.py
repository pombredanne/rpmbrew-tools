from urllib.parse import urlparse, urlsplit, urlunsplit
import requests
import os.path
from bs4 import BeautifulSoup
import re
import sys
from os.path import expanduser
import hashlib
from abc import ABCMeta, abstractproperty
import logging


HAS_WGET = True
HAS_CREATEREPO = True

try:
    from sh import wget
except ImportError as e:
    sys.stderr.write("Warning: wget binary not found. This is needed for downloading")
    HAS_WGET = False
    
try:
    from sh import createrepo
except ImportError as e:
    sys.stderr.write("Warning: crearepo binary not found. You will not be \ "
                     "able to creare repositories")
    HAS_CREATEREPO = False


DEBUG = True

class GetPageMixin(object):
    
    """
    A mixin class that provides a method for downloading pages from 
    the net.
    
    It caches the pages and by default returns them from disk cache
    rather than getting them from the net.
    """
    
    def __get_page__(self, url, purge_age=0):

        if type(url) is str: 
            url_str = url
        elif isinstance(url, URLInfo):
            url_str = url.url
        
        HOME= expanduser("~")
        
        config_dir = os.path.join(HOME, ".config")
        if not os.path.exists(config_dir):
            os.mkdir(config_dir)

        rpmbrew_dir = os.path.join(config_dir, "rpmbrew")
        if not os.path.exists(rpmbrew_dir):
            os.mkdir(rpmbrew_dir)
            
        cache_dir = os.path.join(rpmbrew_dir, "cache")
        if not os.path.exists(cache_dir):
            os.mkdir(cache_dir)
            
    
        filename = hashlib.sha224(str(url).encode()).hexdigest()
        fullpath = os.path.join(cache_dir, filename)
            
        
        if not os.path.exists(fullpath) or DEBUG == True:
            response = requests.get(url_str)
            text = response.text
            
            with open(fullpath, "w") as f:
                f.write(text)
        else:
            with open(fullpath, "r") as f:
                text = f.read()
                return text
            
        
        return text
    
    



class URLInfo(object):
    """
    A class that encapsulates a URL. Python has the urllib.parse.urlpars but 
    I prefer to use a custom class wrapper around it that I  can easily extend 
    in an object oriented way.
    """
    
    def __init__(self, url):
     
        self.__scheme__ = None
        self.__netloc__ = None
        self.__path__ = None
        self.__query__ = None
        self.__fragment__ = None
        
        self.url = url
        
    @property
    def url(self):
        return self.__joined__()
    
    @url.setter
    def url(self, value):
        
        if type(value) is not str:
            value = str(value)
        self.parseresult = urlparse(value)
        self.scheme = self.parseresult.scheme
        self.netloc = self.parseresult.netloc
        self.path = self.parseresult.path
        self.query = self.parseresult.query
        self.fragment = self.parseresult.fragment        
        
    @property
    def scheme(self):
        return self.__scheme__
        
    @scheme.setter
    def scheme(self, value):
        
        self.__scheme__ = value
        
    
    @property 
    def netloc(self):
        return self.__netloc__ 
    
    @netloc.setter
    def netloc(self, value ):
        self.__netloc__ = value
        
        
    @property
    def path(self):
        return self.__path__
    
    @path.setter
    def path(self, value):
        self.__path__ = value
        
    
    @property
    def query(self):
        return self.__query__
        
        
    @query.setter
    def query(self, value):
        self.__query__ = value
        
        
    @property
    def fragment(self):
        return self.__fragment__ 
    
    @fragment.setter
    def fragment(self, value):        
        self.__fragment__ = value
    
    def __joined__(self):
        
        urltuple = (self.scheme, 
                    self.netloc,
                    self.path,
                    self.query,
                    self.fragment)
     
        self.__joined_str__ =  urlunsplit(urltuple)
        
        return self.__joined_str__
    
    def __str__(self):        
        return self.__joined__()
        
        
    def __unicode__(self):        
        return self.__joined__()
 
    def endswith(self, value):
        joined_string = self.__joined__()
        return joined_string.endswith(value)



class RPMUrl(URLInfo):
    
    @property
    def name(self):
        
        return os.path.basename(self.path)
    
    def get_rpm(self):
        #downloads rpm
        pass
        
    def install_rpm(self):
        pass
        
    
    
    
class RPMResource(URLInfo):
    """Extends URLInfo to add downloading the rpm itself."""
    
    
    def save(self):
        pass
    
    def install(self):
        pass
    
    
    def download(self):
        wget(url, "-c")
        
    def curl_download(url):
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.FOLLOWLOCATION, 1)
        c.setopt(pycurl.MAXREDIRS, 5)
    
        # Setup writing
        if os.path.exists(self.name):
            f = open(self.name, "ab")
            c.setopt(pycurl.RESUME_FROM, os.path.getsize(self.name))
        else:
            f = open(self.name, "wb")
            
        c.setopt(pycurl.WRITEDATA, f)
        #c.setopt(pycurl.VERBOSE, 1) 
        #c.setopt(pycurl.DEBUGFUNCTION, test)
        c.setopt(pycurl.NOPROGRESS, 0)
        c.setopt(pycurl.PROGRESSFUNCTION, self.progress)
        try:
            c.perform()
        except:
            pass
    
    def progress(self, *args, **kwargs):
        pass
    
        
class MirrorDirCrawler(GetPageMixin):
    
    """
    Crawls a page listing rpms or srpms,
    Provides property "rpms" which returns a list of RPMResources. 
    
    """
    
    def __init__(self, urlinfo):
        self.urlinfo = urlinfo
        
    def get_rpms(self, regex_string):
        
        results = []
        
        for rpm in self.rpms:
            if re.search(regex_string, rpm.name):
                results.append(rpm)
        
        return results

            
    @property
    def rpms(self): 
        """Returns a list of RPMInfo objects"""
        
        if not hasattr(self, "__rpminfolist__"):
            self.__rpminfolist__ = self.__get_urls__(self.urlinfo)
        return self.__rpminfolist__
        

    def __get_urls__(self, url):
        """Returns a list of the rpms in a page.
        Works with any standard directory index."""
        if not url.endswith("/"):
            url.path = url.path + "/"
                                    
        text = self.__get_page__(url)
        soup = BeautifulSoup(text)    
        hr = soup.find("html")
        body = hr.find("body")
        img = body.findAll("a")
        
        results_list =  []
        for i in img:
            rpm_url = i["href"]
            if rpm_url.endswith(".rpm") or rpm_url.endswith(".srpm"):
                if not rpm_url.startswith("http://"):
                    result = RPMUrl(url)
                    result.path = result.path + rpm_url
                else:
                    result = RPMUrl(url)
                results_list.append(RPMUrl(result))
        return results_list
    
    
class MirrorCrawler:
    """Abstract base class for all MirrorHandler classes. """
    __metaclass__ = ABCMeta
     
    @abstractproperty
    def SRPMS(self):
        raise(NotImplemented("SRPMS not Implemented"))
     
    @abstractproperty
    def RPMS(self):
        raise(NotImplemented("RPMS not Implemented"))


class StandardMirror(MirrorCrawler, GetPageMixin):
    """
    
    """
    
    def __init__(self):
        self.__srpms_list__ =[]    
    

  
class RawhideMirror(MirrorCrawler, GetPageMixin):
    
    """
    
    Gets a list of all RPMS and SRPMS from a rawhide mirror.
    The rawhide SRPMS mirror splits the srpms into different 
    subdirectories hence what we do here is parse the main SRPMS
    page and then crawl each page using MirrorDirCrawler.
    
    
    TODO: Write a more generic class that implements crawling a mirror
    that has the normal main Directory pages. 
    
    """

    def __init__(self):
        
        
        self.logger = logging.getLogger(name=self.__class__.__name__)
        self.__srpms_list__ =[]

    def get_srpms_base(self):
        srpms_base = "http://ftp-stud.hs-esslingen.de/pub/fedora/linux/development/rawhide/source/SRPMS"
        return srpms_base
        
    def get_rpms_base(self):
        rpms_base = "http://ftp-stud.hs-esslingen.de/pub/fedora/linux/development/rawhide/x86_64/os/Packages/"
        return rpms_base
        
    @property
    def SRPMS(self):
        
        if len(self.__srpms_list__) == 0:
            self.__srpms_list__ = self.__srpms_list__()

    
    def __get_srpm_dirs__(self):
        #Start at the base of the SRPMS and get all the links to the subpages
        
        srpms_base = self.get_srpms_base()
        srpms_base_html = self.__get_page__(srpms_base)        
        soup = BeautifulSoup(srpms_base_html)
        
        html = soup.find("html")
        body = html.find("body")
        table = body.find("table")
        tr_all = table.findAll("tr")

        #urlinfo_list is a list of MirrorDir objects.
        
        """Get the mirror urls for each of the page."""
        
        urlinfo_list = []
        
        for tr in tr_all:
            td_all = tr.findAll("td")
            for td in td_all:
                if td.a:
                    text = td.a.text
                    if re.match(r'[\da-z]/', text): 
                        url = URLInfo(srpms_base)
                        url.path = os.path.join(url.path, td.a.text)
                        urlinfo_list.append(url)
                
        for i in urlinfo_list:
            self.logger.debug(i)

        return urlinfo_list

    def __build_srpm_index__(self):                 
        total_list = []       
        
        urlinfo_list = self.__get_srpm_dirs__()
        for urlinfo in urlinfo_list: 
            self.logger.debug("Processing: {}".format(urlinfo))
            md  = MirrorDirCrawler(urlinfo)
            total_list.extend(md.rpms)
            
        #for rpminfo in total_list:
        #    print(rpminfo.name, rpminfo)
            
            
    def __get_rpms_dir__(self):
        pass
        
def search_mirrors(os, version, pattern):
    
    print(os,version,pattern)
    






        
        
        
    
        
        
        
        
        
