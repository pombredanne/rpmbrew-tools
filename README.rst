===============================
rpmbrew-tools
===============================

.. image:: https://img.shields.io/travis/jnvilo/rpmbrew-tools.svg
        :target: https://travis-ci.org/jnvilo/rpmbrew-tools

.. image:: https://img.shields.io/pypi/v/rpmbrew-tools.svg
        :target: https://pypi.python.org/pypi/rpmbrew-tools


A command line toolset that eases building and mocking RPMs.

* Free software: BSD license
* Documentation: https://rpmbrew-tools.readthedocs.org.

Features
--------

* TODO
