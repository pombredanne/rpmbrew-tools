============
Installation
============

At the command line::

    $ easy_install rpmbrew-tools

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv rpmbrew-tools
    $ pip install rpmbrew-tools
